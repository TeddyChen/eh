/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.java.afterjdk7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TryWithResource {

	public void readDate ()  {

		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(new URL("http://teddysoft.tw/").openStream()))) {

			String line = reader.readLine();
			SimpleDateFormat format = new SimpleDateFormat("MM/DD/YY");
			Date date = format.parse(line); // may throw ParseException
			
		} catch (MalformedURLException e) {
			// exception handler
		} catch (IOException e) {
			// exception handler
		} catch (ParseException e) {
			// exception handler
		} 
	}
	
}
