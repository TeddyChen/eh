/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.java.afterjdk7;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class TryMultipleCatch {
    static final String FORMAT_AM_PM = "yyyy/MM/dd aa hh:mm:ss";

    public static Date parseDate (String str){
        Date result;
        SimpleDateFormat formatTW = new SimpleDateFormat(FORMAT_AM_PM, Locale.TAIWAN);

        try {
        		result = formatTW.parse(str); // may throw ParseException
        		URL url = new URL("http://teddysoft.tw/"); // may throw MalformedURLException

        } catch (ParseException | MalformedURLException e) {
        			// exception handler
        		result = new Date();
		}
        return result;
    }
}
