/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.java.beforejdk7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TryCatchFinally {

	public void readDate ()  {
		BufferedReader reader = null;

		try {
			URL url = new URL("http://teddysoft.tw/"); // may throw MalformedURLException
			 reader = new BufferedReader(
					new InputStreamReader(url.openStream())); // may throw IOException
			
			String line = reader.readLine();
			SimpleDateFormat format = new SimpleDateFormat("MM/DD/YY");
			Date date = format.parse(line); // may throw ParseException
			
		} catch (MalformedURLException e) {
			// exception handler
		} catch (IOException e) {
			// exception handler
		} catch (ParseException e) {
			
		} finally {
			if (null != reader)
				try {
					reader.close();
				} catch (IOException e) {
					// log the cleanup exception
				}
		}
	}
}
