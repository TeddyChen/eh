/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.carelesscleanup.ans;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileUtility {
	
	public static void readData(String fileName) {
		FileInputStream fis = null;
		
		try {
				fis = new FileInputStream(new File(fileName));
				// reading data
				fis.close();
		} 
		catch (IOException e) {  
			throw new RuntimeException(e);
		} 
		finally{
			cleanup(fis);
		}
	}
	
	
	public static void readData_using_try_wiht_resources(String fileName) {

		try (FileInputStream fis = new FileInputStream(new File(fileName))) {
				// reading data
		} 
		catch (IOException e) {  
			throw new RuntimeException(e);
		} 
	}
	
	
	private static void cleanup(Closeable in){
	    try {
	    	if (in != null)  in.close();
	    } catch(IOException e) {
	        // log the exception
	   }
	}
}
