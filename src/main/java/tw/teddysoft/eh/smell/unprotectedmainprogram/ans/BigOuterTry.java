/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.unprotectedmainprogram.ans;

public class BigOuterTry {
	private static Dialog dialog;
	private static Logger logger;

	static public void main(String [] args){
		try{
			/*
			 * 做一大堆事情的主程式  
			 */
		} 
		catch (Throwable e){
			logger.log(e);
			dialog.show(Dialog.CRITICAL, e.getMessage());
		}
	}
	
	public class Dialog{
		public static final int CRITICAL = 0;
		public void show(int state, String message) {
			// show the message
		}
	}
	
	public class Logger{
		public void log(Throwable e) {
			// log the throwable
		}
	}
	
}
