/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.sparehandler.exercise;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class ACLManagerTest {

	@Test
	public void introduce_resourceful_try_block() {
		ACLManager acl = new ACLManager();
		
		try {
			acl.readUser("Teddy");
			fail();
		} catch (ReadUserException e) {
			assertTrue(true);
		
		}
	}

}
