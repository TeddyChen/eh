package tw.teddysoft.eh.smell.sparehandler.ans;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.SQLException;

/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
public class ACLManager {
	
	public User readUser(String name) throws ReadUserException{
		final int maxAttempt = 3;
		int attempt = 1;
		while(true) {
			try {
				if(attempt <= 2) return readFromDatabase(name);
				else			return readFromLDAP(name);
			} catch(Exception e){
				if (++attempt > maxAttempt)
					throw new ReadUserException(e);
			}
		}
	}
	
	private User readFromDatabase(String name) throws SQLException{
		if(true) throw new SQLException();
		return null;
	}

	private User readFromLDAP(String name) throws IOException {
		if(true) throw new IOException();
		return null;
	}
}
