/*
 * Copyright 2017 TeddySoft Technology. 
 * 
 */
package tw.teddysoft.eh.smell.ignoredcheckedexception.exercise;


import org.junit.jupiter.api.Test;
import tw.teddysoft.eh.smell.dummyhandler.exercice.UnhandledException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class FileUtilityTest {

	@Test
	public void replace_ignored_checked_exception_with_unchecked_exception() {
		try{
			FileUtility.writeFile(".", "This is a test.");
			fail();
		}
		catch(UnhandledException e){
			assertTrue(true);
		}
	}

}
